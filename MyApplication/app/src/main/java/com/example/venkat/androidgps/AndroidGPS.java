package com.example.venkat.androidgps;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class AndroidGPS extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_gps);
        setUpMapIfNeeded();

        if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    public void findLocation(View v) {
        new GetLatLong().execute();

   }
    public void updateLocation(ArrayList<Double> results){

    }



    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }


    private class GetLatLong extends AsyncTask<Void, Integer, Void> {
        private ArrayList<Double> results = null;
        private String county = "Address not found.";
        @Override
        protected Void doInBackground(Void... params) {
            // Do Whatever
            // Each time you want the progress bar to update, you
            // call the publishProgress method, and pass the value
            // to be passed to the progressBar.setProgress method
           // Toast.makeText(getApplicationContext(), "Async task started.", Toast.LENGTH_SHORT);

            EditText editText = (EditText) findViewById(R.id.text);

            String message = editText.getText().toString();
            results = getLatLong(getLocationInfo(message));

            Log.d("RESULT", "LAT: "+results.get(0)+" LONG: "+results.get(1));

            publishProgress();

            return null;
        }
        public JSONObject getLocationInfo(String address) {
            StringBuilder stringBuilder = new StringBuilder();
            try {

                address = address.replaceAll(" ","%20");

                HttpPost httppost = new HttpPost("http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
                HttpClient client = new DefaultHttpClient();
                HttpResponse response;
                stringBuilder = new StringBuilder();

                response = client.execute(httppost);
                HttpEntity entity = response.getEntity();
                InputStream stream = entity.getContent();
                int b;
                while ((b = stream.read()) != -1) {
                    stringBuilder.append((char) b);
                }
            } catch (ClientProtocolException e) {
            } catch (IOException e) {
            }

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = new JSONObject(stringBuilder.toString());
            } catch (JSONException e) {

                e.printStackTrace();
            }

            return jsonObject;
        }

        public ArrayList<Double> getLatLong(JSONObject jsonObject) {
            double longitude = 0;
            double latitude = 0;

            try {

                longitude = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                latitude = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");


                county = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getString("formatted_address");

                Log.d("COUNTY", county);
            } catch (JSONException e) {


            }
            ArrayList<Double> returnVals = new ArrayList<Double>();
            returnVals.add(latitude);
            returnVals.add(longitude);

            return returnVals;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // update the progress bar. The value you pass in publishProgress
            // is passed in the values parameter of this method
            try {
                MarkerOptions options = new MarkerOptions();
                LatLng position = new LatLng(results.get(0).doubleValue(), results.get(1).doubleValue());
                options.position(position);
                options.title("Da Swole Locator");


                options.snippet("App done by Venkat and Ahmed!");
                SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                GoogleMap googleMap = fm.getMap();
                googleMap.clear();
                googleMap.addMarker(options);

                CameraUpdate updatePosition = CameraUpdateFactory.newLatLng(position);


                CameraUpdate updateZoom = CameraUpdateFactory.zoomBy(6);


                googleMap.moveCamera(updatePosition);


                googleMap.animateCamera(updateZoom);

                mMap.addPolyline((new PolylineOptions()).add(new LatLng(30.28065429999999, -97.7327641),
                        new LatLng(results.get(0), results.get(1))).width(5).color(Color.BLUE));

                MarkerOptions options2 = new MarkerOptions();

                LatLng position2 = new LatLng(30.28065429999999, -97.7327641);
                options2.position(position2);
                options2.title("University of Texas at Austin");


                options2.snippet("Distance: (to be implemented...)");
                SupportMapFragment fm2 = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                GoogleMap googleMap2 = fm2.getMap();
                googleMap2.addMarker(options2);




               
            }
            catch(Exception e){
                Log.d("EXCEPTION", e.getMessage());
            }





            TextView countyDisp = (TextView) findViewById(R.id.textView);
            countyDisp.setText("Full Address: "+ county);
            TextView distDisp = (TextView) findViewById(R.id.textView2);

            Location locUser = new Location("User location");
            Location locUT = new Location("UT");
            locUser.setLatitude(results.get(0).doubleValue());
            locUser.setLongitude(results.get(1).doubleValue());
            locUT.setLatitude(30.28065429999999);
            locUT.setLongitude(-97.7327641);
            double dist = locUser.distanceTo(locUT);
            double distMiles = dist * 0.00062137;

            distDisp.setText("Distance to UT: "+ Math.round(distMiles) + " miles.");

            LocationManager lm3 = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
            Location location3 = lm3.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            double longitudeyou = location3.getLongitude();
            double latitudeyou = location3.getLatitude();

            Location locUser2 = new Location("User location");
            Location locUT2 = new Location("UT");
            locUser2.setLatitude(results.get(0).doubleValue());
            locUser2.setLongitude(results.get(1).doubleValue());
            locUT2.setLatitude(latitudeyou);
            locUT2.setLongitude(longitudeyou);

            double dist2 = locUT2.distanceTo(locUT);
            double distMilesYou = dist2 * 0.00062137;

            TextView userDist = (TextView) findViewById(R.id.textView3);

            userDist.setText("Your distance to UT: "+ distMilesYou + " miles.");

            //App done
        }




    }


}
